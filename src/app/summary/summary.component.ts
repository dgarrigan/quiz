import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-summ',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  @Input() result;
  @Output() resetQuiz = new EventEmitter<boolean>();
  resultMessage: string = '';

  constructor() { }

  ngOnInit() {
    if (this.result.result / this.result.total === 1) { this.resultMessage = 'Well Done!' }
    if (this.result.result / this.result.total <= 0 && this.result.result / this.result.total > 0) { this.resultMessage = 'Please try again...' };
    if (this.result.result / this.result.total === 0) { this.resultMessage = 'Please re-test' }
  }

  reset() {
    this.resetQuiz.emit(true);
  }

}
