import { Component, OnInit } from '@angular/core';
import { QuizService } from './shared/q.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private questions;
  private active_question = 0;
  private progress = {
    first: true,
    val: 0
  };
  private answers = [];
  private finished = false;

  constructor(private service: QuizService) { }

  ngOnInit() {
    this.service.loadQuestions().subscribe((questions) => {
      this.questions = questions;
      this.progress.val = ((this.active_question + 1) / this.questions.questions.length) * 100
    });
  }

  onNavigate(no) {
    if (this.answers.length - 1 >= this.active_question || no < 0) {
      if ((this.active_question > 0 && no < 0) || ((this.active_question < this.questions.questions.length - 1) && no > 0)) {
        this.active_question = this.active_question + no;
        this.progress.val = ((this.active_question + 1) / this.questions.questions.length) * 100;
        this.progress.first = (this.active_question === 0) ? true : false;
      }
    }
    if (this.answers.length === this.questions.questions.length && no > 0) {
      this.finished = true;
    }
  }

  onSelectAnswer($event) {
    this.answers[this.active_question] = $event;
  }

  getResult() {
    const total = this.questions.questions.length;
    const results = this.answers.reduce((sum, item, idx) => {
      if (item === this.questions.questions[idx].correctAnswer) { sum++; }
      return sum;
    }, 0);
    return {
      result: results,
      total: total
    };
  }

  newQuiz($vent) {
    this.active_question = 0;
    this.progress = {
      first: true,
      val: ((this.active_question + 1) / this.questions.questions.length) * 100
    };
    this.answers = [];
    this.finished = false;
  }


}
