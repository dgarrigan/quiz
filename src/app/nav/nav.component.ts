import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavigationComponent {
  @Input() progress;
  @Output() navChanged = new EventEmitter<any>();

  navigate(no) {
    this.navChanged.emit(no);
  }

}
