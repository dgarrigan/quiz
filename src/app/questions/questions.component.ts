import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionComponent {
  @Input() active_question;
  @Input() activePanel;
  @Output() questionSelected = new EventEmitter<number>();

  onPanelSelect(i) {
    this.activePanel = i;
    this.questionSelected.emit(i);
  }
}
